"use strict";

var app = angular.module('timeTracker', ['angular-websql'])
.run(function ($rootScope, $timeout,$webSql) {
  $rootScope.NW = require('nw.gui');
  $rootScope.tray = new $rootScope.NW.Tray({title: 'Tray', icon: 'flat-avatar.png'});

  var nw = $rootScope.NW;
  var win = nw.Window.get();
  var tray = $rootScope.tray;
  var menu = new nw.Menu({type: "menubar"});
  $rootScope.mainWin = win;

  if (process.platform == "darwin")
  {
    menu.createMacBuiltin && menu.createMacBuiltin('Time Tracker');
  }
  
    // Create and append the 1st level menu to the menubar
    menu.append(new nw.MenuItem({
      label: 'Task Report',
      //submenu: submenu
        click: function()
        {
          nw.Window.open('views/task/taskreport.html',{toolbar:false});
        }
    }));

    menu.append(new nw.MenuItem({
      label: 'Exit',
      //submenu: submenu
      click:function(){
        nw.App.quit();
      }
    }));

    tray.menu = menu;
    win.on('close',function(){
      this.hide();
    });

    win.on('minimize', function() {
      this.hide();
    });

    tray.on('click',function(){

     
     win.reload();
     win.show();


    });

    if ($webSql.openDatabase) {
              //Create the database the parameters are 1. the database name 2.version number 3. a description 4. the size of the database (in bytes) 1024 x 1024 = 1MB
              $rootScope.mydb = $webSql.openDatabase("task_db", "0.1", "A Task Database", 1024 * 1024);


              $rootScope.mydb.createTable('task',{

              "taskname":{
                "type": "TEXT"
              },
              "priority": {
                "type": "TEXT"
              },
              "starttime": {
                "type": "TEXT"
              },
              "endtime": {
                "type": "TEXT"
              },
              "reminder": {
                "type": "TEXT"
              },
              "taskdetails": {
                "type": "TEXT"
              },
               "status": {
                "type": "TEXT"
              },
              "comments": {
                "type": "TEXT"
              },
              "totaltime":{
                "type": "TEXT"
              }
          });


            } else {
              alert("WebSQL is not supported by your browser!");
            }

          });

app.controller("ParentCtrl",ParentCtrlFunction);

function ParentCtrlFunction($scope,$rootScope,$timeout,$webSql,$filter)
{
     
     
         $scope.parentProperty = 'parent scope';
         $scope.todayDateTime = Date.now();
         var tempTodayDateTime = $filter('date')(new Date(), 'yyyy-MM-dd hh:mm:ss a');
        // $scope.disableflag = "false";
//$scope.taskReportList=[];

console.log(tempTodayDateTime);


        $scope.mydb = $rootScope.mydb;

        //To drop the table;
        //$scope.mydb.dropTable('task');
        getData($scope.mydb);
        getDataList($scope.mydb);

        console.log("Task--->"+$scope.task);

        $scope.priorities =[
        {id : '1',name : 'Low'},
        {id : '2',name : 'Normal'},
        {id : '3',name : 'High'}
        ];
        $scope.reminders =[

        {id : '1',value : '3'},
        {id : '2',value : '30'},
        {id : '3',value : '60'},
        {id : '4',value : '90'},
        {id : '5',value : '120'}
        ];

        var taskstatus = [{'running':'R', 'completed':'C'}
        ];

        $scope.saveTask = function(){

          console.log("Enter the save task");   
         var tempMyDB = $scope.mydb;
         if (tempMyDB) {                
                var tempTask = $scope.task;
                //Test to ensure that the user has entered both a make and model
                if (tempTask !== "" && tempTask !=null) {
                    
                    //Insert into table.
                    tempMyDB.insert('task', {"taskname": tempTask.taskname, "priority": tempTask.priority, 'starttime': tempTodayDateTime, 'reminder': tempTask.reminder, 'taskdetails': tempTask.taskdetails,'status': 'R' }).then(function(results) {
                        console.log('Record inserted successfully.......');
                         //$scope.disableflag = false;
                    })

                    $timeout(function() {
                      $rootScope.mainWin.reload();
                      $rootScope.mainWin.show();
                    }, tempTask.reminder*60000);
                  } else {
                    alert("You must enter a task!");
                  }
                } else {
                  alert("db not found, your browser does not support web sql!");
                }
                 $scope.task = [];
                 hideWindow();
                 console.log("Ending the save task.");
              };

              $scope.continueTask = function(){
                 console.log("Enter the continueTask. ");   
                 var tempMyDB = $scope.mydb;
                 if (tempMyDB) {                
                var tempTask = $scope.task;
                //Test to ensure that the user has entered both a make and model


                if (tempTask !== "" && tempTask !=null) {
                    
                    //Insert into table.     

                     var tempDateDif = new Date(tempTodayDateTime) - new Date(tempTask.starttime); 
                     var diffInMins = Math.round(((tempDateDif % 86400000) % 3600000) / 60000); 

                      
                    $timeout(function() {
                       $rootScope.mainWin.reload();
                       $rootScope.mainWin.show();
                    }, 3000);
                  } else {
                    alert("You must enter a task!");
                  }
                } else {
                  alert("db not found, your browser does not support web sql!");
                }
                 $scope.task = [];
                 hideWindow();
                 console.log("Ending the continueTask.");
              }

              $scope.closeTask = function(){
                 console.log("Enter the continueTask. ");   
                 var tempMyDB = $scope.mydb;
                 if (tempMyDB) {                
                var tempTask = $scope.task;
                //Test to ensure that the user has entered both a make and model
                if (tempTask !== "" && tempTask !=null) {
                    var tempDateDif = new Date(tempTodayDateTime) - new Date(tempTask.starttime); 
                    var diffInMins = Math.round(((tempDateDif % 86400000) % 3600000) / 60000); 
                    //Insert into table.                 
                      tempMyDB.update("task", {"endtime" : tempTodayDateTime , 'status': 'C', "totaltime": diffInMins}, {
                        "taskname": tempTask.taskname
                      });

                    // $timeout(function() {
                    //   $rootScope.mainWin.reload();
                    //   $rootScope.mainWin.show();
                    // }, 3000);
                  } else {
                    alert("You must enter a task!");
                  }
                } else {
                  alert("db not found, your browser does not support web sql!");
                }
                 $scope.task = [];
                 hideWindow();
                 console.log("Ending the continueTask.");
              }

              function hideWindow()
              {
                $rootScope.NW = require('nw.gui');
                 var nw = $rootScope.NW;
                 var win = nw.Window.get();
                 win.hide();
               };

               function getData(db)
               {
                db.selectAll("task").then(function(results) {
                  $scope.task = {taskname:"",
                                priority:"",
                                starttime:"",
                                endtime:null,
                                reminder:"",
                                taskdetails:"",
                                status:""};
                 // results = JSON.parse(results);
                  for(var i=0; i < results.rows.length; i++){
                    if(results.rows.item(i).status=='R')
                    {
                        //$scope.task.push(results.rows.item(i));
                        $scope.task = results.rows.item(i);
                        $scope.disableflag="true";
                        console.log($scope.task);
                    }

                  }
                });
               };

               function getDataList(db)
               {
                db.selectAll("task").then(function(results) {
                  $scope.taskReportList = [];
                  for(var i=0; i < results.rows.length; i++){
                    //var second = new Date(results.rows.item(i).endtime) - new Date(results.rows.item(i).starttime);
                    //console.log("Second-->"+second);
                   // results.rows.item(i).totaltime = second;
                    $scope.taskReportList.push(results.rows.item(i));
                    //$scope.taskReportList.totaltime = second;
                  }
                });
               };
            }

